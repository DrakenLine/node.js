const express = require('express')
const app = express()
const axios = require('axios')

const bodyParser = require('body-parser')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
 
app.get('/users', async function (req, res) {
  try{
  	const {data} = await axios.get('https://jsonplaceholder.typicode.com/users');
  	console.log({data});
  	res.send({data})

  }catch(error){
  	console.error(error);
  }

})

app.get('/users/:id', async function (req, res) {
  try{
  	let id = req.params.id;
  	const {data} = await axios.get('https://jsonplaceholder.typicode.com/users/'+id);
  	console.log({data});
  	res.send({data})

  }catch(error){
  	console.error(error);
  }
})

app.post('/users', async function (req, res) {
  try{
  	name: "Maurice"
  	username: "Jévin"
  	email: "toto@toto.com"
  	console.log("User ajouté !");
  	res.send("User ajouté !");

  }catch(error){
  	console.error(error);
  }
})

app.put('/users/:id', async function (req, res) {
  try{
  	let id = req.params.id;
  	const {data} = await axios.put('https://jsonplaceholder.typicode.com/users/'+id, {
  		name: "Maurice",
  		username: "Lolita",
  		age: 66
  	});
  	
  	console.log({data});
  	res.send({data});

  }catch(error){
  	console.error(error);
  }
})

app.delete('/users/:id', async function (req, res) {
  try{
  	let id = req.params.id;
  	const {data} = await axios.delete('https://jsonplaceholder.typicode.com/users/'+id+'');
  	
  	console.log({data});
  	res.send("Utilisateur décédé :3");

  }catch(error){
  	console.error(error);
  }
})

app.get('/users/:id/posts', async function (req, res) {
  try{
  	let id = req.params.id;
  	const {data} = await axios.get('https://jsonplaceholder.typicode.com/users/'+id+'/posts');
  	console.log({data});
  	res.send({data});

  }catch(error){
  	console.error(error);
  }
})

app.get('/users/:id/albums', async function (req, res) {
  try{
  	let id = req.params.id;
  	const {data} = await axios.get('https://jsonplaceholder.typicode.com/users/'+id+'/albums');
  	console.log({data});
  	res.send({data})

  }catch(error){
  	console.error(error);
  }
})

//je n'y suis pas arrivée :(
app.get('/photos', async function (req, res) {
  try{
  	let userId = req.params.userId;
    axios.get('https://jsonplaceholder.typicode.com/users/'+userId+'/albums');
    let albumId = req.params.albumId;
      for (var i = 0; i < 10; i++) {
        const {data} = await axios.get(
          axios.get('https://jsonplaceholder.typicode.com/photos/albumId/'+albumId[i])
        );

      console.log({data});
      res.send({data});
      }

      // const [res1, res2] = await axios.all([
      //       axios.get('https://jsonplaceholder.typicode.com/users/'+userId+'/albums'),
      //       axios.get('https://jsonplaceholder.typicode.com/photos/albumId/'+albumId[i])
      //   ]);
      //   console.log(res1.data);
      //   console.log(res2.data);

  }catch(error){
  	console.error(error);
  }
})
 
app.listen(3000)